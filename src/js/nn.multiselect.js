
(function($nn) {
	$nn.fn.extend({

		/**
		 * Show a nicer multiselect-dropdown
		 *  
		 * ```
		 * <select data-nn-multiselect multiple="multiple"></select>
		 * 
		 * $nn('select').multiselect();
		 * ```
		 * 
		 */
		'multiselect': function ( opt ) {

			return this.each(function () {

				var $select = $nn(this);
				
				var placeholder = $select.attr('title');

				var $ui = $nn('\
					<div class="nn-multiselect">\
						<div class="nn-multiselect-current nn-form-control">\
							<span class="nn-multiselect-placeholder">' + placeholder + '</span>\
						</div>\
					</div>\
				');
				
				var $uiCurrent = $ui.find('.nn-multiselect-current');

				$select.click(function () {
				}, function () {
				});

				function build() {
					remove();
					var vals = $select.val();

					var $template = $nn('<ul class="nn-multiselect-dropdown"></ul>');
					$select.find('option').each(function ( $el ) {
						$el = $nn($el);
						var label = $el.text();
						var val = $el.attr('value') === '' ? '' : ($el.attr('value') || label);
						var checked = vals.indexOf(val) > -1 ? 'checked="checked"' : '';
						var $row = $nn('\
							<li>\
								<label>\
									<input type="checkbox" ' + checked +  ' value="' + val + '">\
									<span>' + label + '</span>\
								</label>\
							</li>\
						');
						$row.find('input').change( onClickOption );
						$template.append($row);
					});

					$ui.append($template);

					$select.hide();
					
					$ui.find('.nn-multiselect-current').click(function () {
						$ui.toggleClass('in');
					});

					$select.parent().append($ui);
				}

				/**
				 * Close dropdown on click outside of UI
				 * 
				 */
				$ui.clickOutside(function ( e ) {
					$ui.removeClass('in');
				});

				/**
				 * Change of `<select>` value:
				 * Update list of pills and checkboxes in UI
				 * 
				 */
				$select.change( function () {
					
					var vals = $select.val();
					$ui.toggleClass( 'has-selection', vals.length > 0 );
					$ui.find('.nn-multiselect-pill').remove();
					
					var $dropdown = $ui.find('.nn-multiselect-dropdown');
					$dropdown.find('input').removeAttr('checked');

					vals.forEach(function ( val ) {
						var $option = $select.find('[value="' + val + '"]');
						var label = $option.text();
						var $pill = $nn('<span class="nn-multiselect-pill">' + label + '</span>');
						
						$dropdown.find('[value="' + val + '"]').attr({checked:'checked'});

						$pill.click(function () {
							vals = vals.filter( function ( v ) {
								return v != val;
							});
							$select.val( vals );
							return false;
						});
						$uiCurrent.append( $pill );
					});
				});

				function remove() {
					$select.parent().find('.nn-multiselect').remove();
					$select.show();
				}

				function onClickOption() {
					var $me = $nn(this);
					var myVal = $me.val();
					var vals = $select.val();
					vals = vals.filter( function ( val ) {
						return val != myVal;
					});
					if (this.checked) {						
						vals.push( myVal );
					}
					$select.val( vals );
				}

				build();
				$select.change();

				return this;
			});
		}
	});

	/**
	 * 
	 */
	$nn(function () {
		$nn('[data-nn-multiselect]').multiselect();
	});
	
})(window.$nn);
