
/**
 * Easing functions.
 * 
 * To add more easing-function see https://easings.net/de
 * 
 * ```
 * var obj = {a:0};
 * NnEasing.tween(obj, {a:1}, {duration:2000, easing:NnEasing.easeOutBack})
 * 	.step( function () { ... })
 * 	.start( function () { ... })
 * 	.done( function () { ... })
 * ```
 */
 var NnEasing = function() {};

 NnEasing.easeOutBack = function (x) {
	 var c1 = 1.70158;
	 var c3 = c1 + 1;		
	 return 1 + c3 * Math.pow(x - 1, 3) + c1 * Math.pow(x - 1, 2);
 }
 
 NnEasing.easeOutQuint = function (x) {
	 return 1 - Math.pow(1 - x, 5);
 }
 
 NnEasing.tween = function ( obj, params, options ) {
	 if (obj._tween) {
		 obj._tween.stop();
	 }
	 obj._tween = (new NnEasing()).init( obj, params, options );
	 return obj._tween;
 }
 
 NnEasing.prototype.utc = function () {
	 return (new Date()).getTime();
 }
 
 NnEasing.prototype.init = function ( obj, params, options ) {
	 this.obj = obj;
	 this.objClone = {};
	 for (var i in obj) {
		 this.objClone[i] = obj[i];
	 }
	 this.params = params;
	 this.stopped = false;
	 this.started = false;
 
	 this.time = this.utc();
	 this.options = options;
	 this.tick();
 
	 return this;
 }
 
 NnEasing.prototype.step = function ( callback ) {
	 this.onStep = callback;
 }
 
 NnEasing.prototype.start = function ( callback ) {
	 this.onStart = callback;
 }
 
 NnEasing.prototype.done = function ( callback ) {
	 this.onDone = callback;
 }
 
 NnEasing.prototype.tick = function () {
	 var delay = this.options.delay || 0;
	 var dur = this.options.duration;
	 var easing = this.options.easing || NnEasing.easeOutQuint;
 
	 if (!this.stopped) {
		 requestAnimationFrame( this.tick.bind(this) );
	 }
 
	 var p = this.utc() - delay - this.time;		
	 if (p < 0) return;
 
	 if (!this.started) {
		 this.started = true;
		 if (this.onStart) {
			 this.onStart( this.obj );
		 }
	 }
 
	 p = 1/dur * Math.max(Math.min(p, dur), 0);		
	 if (p >= 1) {
		 this.stop();
		 if (this.onDone) {
			 this.onDone( this.obj );
		 }
	 }
 
	 for (var i in this.params) {
		 var dif = this.params[i] - this.objClone[i];
		 this.obj[i] = this.objClone[i] + dif * easing(p);
	 }
 
	 if (this.onStep) {
		 this.onStep( this.obj );
	 }
 
	 return this;
 }
 
 NnEasing.prototype.stop = function () {
	 delete( this.obj._tween );
	 this.stopped = true;
 }
