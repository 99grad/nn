
(function($nn) {
	$nn.fn.extend({

		/**
		 * Show a tooltip
		 * 
		 * Use `data-nn-tooltip` to automatically initialize:
		 * ```
		 * <a data-nn-tooltip title="text">...</a>
		 * ```
		 * 
		 * Or initialize the tooltip manually:
		 * ```
		 * <a title="text">...</a>
		 * $nn('button').tooltip();
		 * ```
		 * 
		 */
		'tooltip': function ( opt ) {

			return this.each(function () {

				var $el = $nn(this);
				
				$el.hover(function () {
					var title = $el.attr('title');
					var $tooltip = $nn('<div class="nn-tooltip">' + title + '</div>');
					var offset = $el.offset();
					var w = $el.width();
					$tooltip.position({top:offset.top, left:offset.left + w/2}).delay(1).addClass('in');
					$nn('body').append($tooltip);
					$el.data({$nnTooltip:$tooltip});
				}, function () {
					var $tooltip = $el.data().$nnTooltip;
					if ($tooltip) {
						$tooltip.removeClass('in').delay(500).remove();
						$el.data({$nnTooltip:false});
					}
				});

				return this;
			});
		}
	});

	/**
	 * 
	 */
	$nn(function () {
		$nn('[data-nn-tooltip]').tooltip();
	});
	
})(window.$nn);
